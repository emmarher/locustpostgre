from __future__ import absolute_import
from __future__ import print_function
from distutils.util import execute
from sqlalchemy import create_engine
from sqlalchemy.engine import url
from locust import User, between, TaskSet, task, events
import time

def create_connection():
    print("Connecting to PostgreSQL database...")
    database_connection_params = {
        'drivername': 'postgres+pg8000',
        'host': 'localhost',
        'port': 5432,
        'username': 'postgres',
        'password': '1Q2W3E-1q2w3e',
        'database': 'locustTest'    
    }
    return create_engine(url.URL(**database_connection_params)).connect()

def execute_query(query):
    _conn = create_connection()
    rs = _conn.execute(query)
    return rs

"""
    The PostgreSQL client that wraps the actual query
"""
class PostgreSqlClient():
    
    def __getattr__(self, name):
        def wrapper(*args, **kwargs):
            start_time = time.time()
            try:
                res = execute_query(*args, **kwargs)
                events.request_success.fire(request_type='pg8000', 
                                            name=name, 
                                            response_time=int((time.time() - start_time) * 1000),
                                            response_length=res.rowcount)
            except Exception as e:
                events.request_failure.fire(request_type='pg8000', 
                                            name=name, 
                                            response_time=int((time.time() - start_time) * 1000), 
                                            exception=e)
                print('Error {}'.format(e))
        return wrapper
    
class CustomTaskSet(TaskSet):
    @task(1)
    def execute_query(self):
        self.client.execute_query("select * from table1")

# This clas will be executed when you fire up locust.
class PostgreLocust(User):
    min_wait = 0
    max_wait = 0
    task_set = CustomTaskSet
    wait_time = between(min_wait, max_wait)
    
    def __init__(self, environment):
        super().__init__(environment)
        self.client = PostgreSqlClient()
    