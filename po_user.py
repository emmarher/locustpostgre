

#from bson.codec_options import TypeRegistry, CodecOptions
from faker import Faker
from locust import User, between, task, tag, events

#from decimal_codec import DecimalCodec
from settings import DEFAULTS

import time
import psycopg2
from psycopg2 import Error


        
def postgre_task(weight=1, batch_size=1):
    """ A decorator for postgre_task that takes a weight parameters"""       
    
    def middle(func):
        @task(weight=weight)
        def run_postgre_opertaion(self):
            start_time = time.time()
            # display the function name as operation name reported in locust stats
            name = func.__name__
            try:
                func(self)
            except Exception as e:
                # output the error for debugging purposes
                print(e)
                total_time = iny((time.time() - start_time) * 1000)
                for x in range(batch_size):
                    self.environment.events.request_failure.fire(request_type='psycopg2', 
                                                                 name=name, 
                                                                 response_time=total_time,
                                                                 exception=e,
                                                                 response_length=0,
                    )
                else:
                    total_time = int((time.time() - start_time) * 1000)
                    for _ in range(batch_size):
                        self.environment.events.request_failure.fire(request_type='psycopg2', 
                                                                     name=name, 
                                                                     response_time=total_time,
                                                                     response_length=1
                        )
            return run_postgre_opertaion
        
        return middle
        
class PostgresUser(User):
    """
    Base postgres workload generator, setting connection parameters
    """
    # this class needs to be marked as abstract, otherwise locust will try to instantiate it
    abstract = True
    
    # no think time between calls
    
    wait_time = between(0.0, 0.0)
    
    
    def __init__(self, environment):
        super().__init__(environment)
        self.request_type = 'psycopg2'
        
        self.connection = psycopg2.connect(user=DEFAULTS['user'],
                            password=DEFAULTS['password'],
                            host=DEFAULTS['host'],
                            port=DEFAULTS['port'],
                            database=DEFAULTS['dbname'])
        print('Se creo la conexion.........')
        self.faker = Faker()
        
    def __getattr__(self, name):
        def wrapper(*args, **kwargs):
            start_time = time.time()
            try:
                result = self.connection.execute(*args, **kwargs).fetchall()
            except Exception as e:
                print('Exception occurred: ' + str(e))
                total_time = int((time.time() - start_time) * 1000)
                events.request_failure.fire(request_type=self.request_type, name=name, response_time=total_time,
                                            exception=e)
            else:
                total_time = int((time.time() - start_time) * 1000)
                events.request_success.fire(request_type=self.request_type, name=name, response_time=total_time,
                                            response_length=len(str(result)))
        return wrapper
        

        
