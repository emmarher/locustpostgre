import os
from unittest.mock import DEFAULT

DEFAULTS = {'dbname': 'locustTest',
            'user': 'postgres',
            'password': '5563-25f2-4499-814',
            #'password': '1Q2W3E-1q2w3e',
            #'host': 'localhost',
            'host': '10.40.30.32',
            'port':5432,}

def init_defaults_from_env():
    for key in DEFAULTS.keys():
        value = os.environ.get(key)
        if value:
            DEFAULTS[key] = value
            
# get the settings from the environment variables
init_defaults_from_env()