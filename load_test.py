
from po_user import PostgresUser, postgre_task
from locust import between

import random

NAMES_TO_CACHE = 1000
class PostgreSampleUser(PostgresUser):
    
    wait_time = between(0,0)
    
    def __init__(self, environment):
        super().__init__(environment)
        self.name_cache = []
        
        
    def generate_new_user(self):
            
        nuser = {
            'first_name': self.faker.first_name(),
            'last_name': self.faker.last_name(),
            'address': self.faker.address(),
            'city': self.faker.city()
        }
            
        print('NEW USER: ', nuser)
        return nuser
        
    def on_start(self):
        self.name_cache = []
            
    def on_stop(self):
        self.connection.close()
        
    @postgre_task()
    def insert_new_user(self):
        usuario = self.generate_new_user()
        cached_names = (usuario['first_name'], usuario['last_name'])
        if len(self.name_cache) < NAMES_TO_CACHE:
            self.name_cache.append(cached_names)
        else:
            if random.randint(0, 9) == 0:
                self[random.randint(0, len(self.name_cache) - 1)] = cached_names
                
        
        query = "INSERT INTO table1 (first_name, last_name, address, city) VALUES(%s,%s, %s, %s)"
        self.connection.cursor().execute(query, (usuario['first_name'], usuario['last_name'], usuario['address'], usuario['address']))
        
        self.connection.commit()