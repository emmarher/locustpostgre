from functools import wraps

from settings import DEFAULTS

import time
import psycopg2
from psycopg2 import Error

try:
    
    # Connect to your postgres DB
    conn = psycopg2.connect(user=DEFAULTS['user'],
                            password=DEFAULTS['password'],
                            host=DEFAULTS['host'],
                            port=DEFAULTS['port'],
                            database=DEFAULTS['dbname'])
    
    # CreaTE A CRUSOR TO PERFORM DATABASE OPERATIONS
    cursor = conn.cursor()
    # Print PostgreSQL details
    print("PostgreSQL server information")
    print(conn.get_dsn_parameters,"\n")
    #Executing a SQL statement
    #cursor.execute("SELECT version();")
    
    insert_query = """INSERT INTO table1 (first_name, last_name, address, city) VALUES ('John','Leuna', 'Playa tulum233', 'Queretaro')"""
    nuser = {
            'first_name': 'Emmanuel',
            'last_name': 'Martinez',
            'address': 'playa',
            'city': 'Altamirano'
        }
    insert_multi_query = """INSERT INTO table1 (first_name, last_name, address, city) VALUES ('John','Leuna', 'Playa tulum233', 'Queretaro'), ('Emma','Leuna', 'Playa tulum233', 'Guanajuato'), (%s,%s, %s, %s)"""
    
    cursor.execute(insert_multi_query, (nuser['first_name'], nuser['last_name'], nuser['address'], nuser['address']))
    #Cuando se insertar valores
    conn.commit()
    print('Inserted data')
    
    cursor.execute("SELECT * FROM table1;")
    # Fetch result
    #record = cursor.fetchone()
    record = cursor.fetchall()
    print("Recorded::::>> -" , record, "\n")
except (Exception, Error) as error:
    print("Error while connecting to PostgreSQL", error)
finally:
    if(conn):
        cursor.close()
        conn.close()
        print("PostgreSQL connection is closed")