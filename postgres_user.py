from functools import wraps
from random import randint

#from bson.codec_options import TypeRegistry, CodecOptions
from faker import Faker
from locust import User, between, task, tag, events

#from decimal_codec import DecimalCodec
from settings import DEFAULTS

import time
import psycopg2
from psycopg2 import Error


        
class PostgresUser(User):
    """
    Base postgres workload generator, setting connection parameters
    """
    abstract = True
    
    def __init__(self, environment):
        super().__init__(environment)
        self.request_type = 'psycopg2'
        self.table_name = 'table1'
        self.connection = psycopg2.connect(user=DEFAULTS['user'],
                            password=DEFAULTS['password'],
                            host=DEFAULTS['host'],
                            port=DEFAULTS['port'],
                            database=DEFAULTS['dbname'])
        print('Se creo la conexion.........')
        self.faker = Faker()
        
    def __getattr__(self, name):
        def wrapper(*args, **kwargs):
            start_time = time.time()
            try:
                result = self.connection.execute(*args, **kwargs).fetchall()
                print(name)
            except Exception as e:
                print('Exception occurred: ' + str(e))
                total_time = int((time.time() - start_time) * 1000)
                events.request_failure.fire(request_type=self.request_type, name=name, response_time=total_time,
                                            exception=e)
            else:
                total_time = int((time.time() - start_time) * 1000)
                events.request_success.fire(request_type=self.request_type, name=name, response_time=total_time,
                                            response_length=len(str(result)))
        return wrapper
        

        
class PostgresSampleUser(PostgresUser):
    """_summary_

    Generic sample postgresql workload generator
    """
    
    wait_time = between(0.0,0.0)
    
    def __init__(self, environment):
        super().__init__(environment)
        self.name_cache = []
        
    def generate_new_user(self):
        """_summary_

        Returns:
            nuser: object that contains the data of the new user created
        """
        nuser = {
            'first_name': self.faker.first_name(),
            'last_name': self.faker.last_name(),
            'address': self.faker.address(),
            'city': self.faker.city()
        }
        #print('NEW USER: ', nuser)
        return nuser
    
    def on_start(self):
        self.name_cache = []
        self.cursor = self.connection.cursor()
        
    def on_stop(self):
        self.cursor.close()
        self.connection.close()

 
    @task(3)
    def insert_single_user(self):
        usuario = self.generate_new_user()
        query = "insert into table1 (first_name, last_name, address, city) values(%s,%s, %s, %s)"
        
        
        self.connection.cursor().execute(query, (usuario['first_name'], usuario['last_name'], usuario['address'], usuario['city']))
        self.connection.commit()
        #self.connection.cursor.close()
        #self.connection.close()

    @task(1)
    def insert_many_user(self):
        usuario = self.generate_new_user()
        usuario2 = self.generate_new_user()
        usuario3 = self.generate_new_user()
        usuario4 = self.generate_new_user()
        usuario5 = self.generate_new_user()
        usuario6 = self.generate_new_user()
        query = "insert into table1 (first_name, last_name, address, city) values(%s,%s, %s, %s),(%s,%s, %s, %s),(%s,%s, %s, %s),(%s,%s, %s, %s),(%s,%s, %s, %s),(%s,%s, %s, %s)"
        
        
        self.connection.cursor().execute(query, (usuario['first_name'], usuario['last_name'], usuario['address'], usuario['city'], 
                                                 usuario2['first_name'], usuario2['last_name'], usuario2['address'], usuario2['city'],
                                                 usuario3['first_name'], usuario3['last_name'], usuario3['address'], usuario3['city'],
                                                 usuario4['first_name'], usuario4['last_name'], usuario4['address'], usuario4['city'],
                                                 usuario5['first_name'], usuario5['last_name'], usuario5['address'], usuario5['city'],
                                                 usuario6['first_name'], usuario6['last_name'], usuario6['address'], usuario6['city']))
                                                
        self.connection.commit()
               

        
    @task(2)
    def select_by_last_name(self):
        

        query = "select first_name, last_name from table1 where last_name like  '%ez'"

        #cursor = self.connection.cursor()
        self.cursor.execute(query)
        r = self.cursor.fetchall()
        #print(r)
        #cursor.close()
        #self.connection.cursor().execute(query).fetchall()
        #self.connection.cursor().fetchall() 

        
    @task(2)
    def select_by_id(self):
        
        query = "select first_name, last_name from table1 where id between 1 and 1000;"
        self.cursor.execute(query)
        self.cursor.fetchall()
        #self.connection.cursor().execute(query)
        #self.connection.cursor().fetchall()
        